# Removes all the docker layers which are orphans
docker_remove_orphan_layers() {
    docker rmi $(echo -n `d images|grep none`|sed  's/<none> <none>/\n/mg'|cut -d ' ' -f 2)
}

docker_up() {
    docker run --rm -it \
           -p 2181:2181 -p 3030:3030 -p 8081:8081 \
           -p 8082:8082 -p 8083:8083 -p 9092:9092 \
           -e ADV_HOST=127.0.0.1 \
           -v $KAFKA_DATA_PATH:/var/lib/kafka/data \
           landoop/fast-data-dev
}
