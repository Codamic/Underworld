
setup_fs() {
    export KAFKA_DATA_PATH="$META_PATH/volumes/kafka/"
    mkdir -p $KAFKA_DATA_PATH
}

cleanup_fs() {
    sudo rm -rf "$META_PATH/volumes"
}
