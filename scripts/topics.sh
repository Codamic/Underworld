create_topic() {
    echo "Creating '$1' topic..."
    docker run --rm -it --net=host landoop/fast-data-dev bash /usr/local/bin/kafka-topics \
           --zookeeper localhost:2181 \
           --create \
           --partitions 2 \
           --replication-factor 1 \
           --topic $1
}

list_topics() {
    docker run --rm -it --net=host landoop/fast-data-dev bash /usr/local/bin/kafka-topics \
           --zookeeper localhost:2181 \
           --list
}

delete_topic() {
    echo "Creating '$1' topic..."
    docker run --rm -it --net=host landoop/fast-data-dev bash /usr/local/bin/kafka-topics \
           --delete \
           --zookeeper localhost:2181 \
           --if-exists \
           --topic "$1"
}

setup_topics() {
    for i in $(cat $1)
    do
        create_topic $i
    done
}
