# Meta

## Requirements

* Docker >= 1.13
* Docker Compose

## Usage
Use the `meta` script to do your shit. There are comments for each subcommand of `meta` so take a look at
the script first. It's pretty straight forward.

In order to run the environment use:

```bash
$ ./meta up
```
For stoping the dev env just press `Ctrl-C`.

In order to teardown the env:

```bash
$ ./meta cleanup
```

In order to get a bash to the kafka container ( to use kafka utilities ):

```bash
$ ./meta console
```
It will brings up a bash for you.

### Kafka commands

In order to create a topic use:

```bash
$ ./meta create-topic sometopic-name
```
list the current topics:

```bash
$ ./meta list-topics
```

to remove a topic:

```bash
$ ./meta delete-topic topic-name
```

in order to setup the kafka broker with out topics (A set of topics which we need to operate) use:

```bash
$ ./meta setup-topics
```

If you need to create a topic as part of the system, something that is going to be permanent add the
topic name to `config/topics` and run the above command. Also make sure to add the description
about the replication factor and other details to `./topics.md`.
